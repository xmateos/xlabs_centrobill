<?php

namespace XLabs\CentroBillBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use XLabs\CentroBillBundle\Entity\FeedRequest;
use XLabs\CentroBillBundle\API\CentroBill as CentroBillApi;
use \DateTime;
use XLabs\CentroBillBundle\Entity\Feed as FeedEntry;
use \Exception;
use XLabs\CentroBillBundle\Event\Feed\Update as FeedUpdated;
use XLabs\CentroBillBundle\Event\Actions\TrialToFull;

class FeedCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:centrobill:feed')
            ->setDescription('Pulls centrobill transactions feed and runs operations, like trial to full, that dont come in IPN format.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine')->getManager();

        /*
         * Instead of using "dateFrom" and "dateTo" params, that can return a huge amount of data (translated into a 500 error), CB guys recommend using lastTransactionId or lastUuid parameters (you just need to send the last id you've reconciled (or have firm data on))
         */

        $config = $container->getParameter('xlabs_centrobill_config');
        $date = new DateTime();
        $feed_params = [
            'siteName' => $config['site_name'][0],
            'loadAll' => 1,
            'dateFrom' => $date->modify('-1 day'),
            'dateTo' => $date
        ];
        $feed = new FeedRequest($feed_params);
        $data = $container->get(CentroBillApi::class)->getFeed($feed->toCleanArray());

        foreach($data['data'] as $row)
        {
            $entry = new FeedEntry($row);
            $existing_row = $em->getRepository(FeedEntry::class)->find($entry->__get('id'));
            if(!$existing_row)
            {
                $em->persist($entry);
                try {
                    $em->flush();

                    if($row['payment']['mode'] == 'settle' && $row['payment']['status'] == 'success')
                    {
                        $event = new TrialToFull($row);
                        $dispatcher = $container->get('event_dispatcher');
                        $dispatcher->dispatch($event::NAME, $event);
                    }
                } catch(Exception $e) {
                    //dump($e); die;
                }
            }
        }

        $event = new FeedUpdated();
        $dispatcher = $container->get('event_dispatcher');
        $dispatcher->dispatch($event::NAME, $event);
    }
}