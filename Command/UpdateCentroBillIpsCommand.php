<?php

namespace XLabs\CentroBillBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UpdateCentroBillIpsCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs:centrobill:update_ips')
            ->setDescription('Pulls centrobill IPs to allow transactions from them.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();

        $url = 'https://api.centrobill.com/ips';

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => "https://api.centrobill.com/ips",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "accept: application/json"
            ],
        ]);

        $response = curl_exec($ch);
        if(curl_error($ch))
        {
            mail('xavi.mateos@manicamedia.com', 'CentroBill IPs updater ('.date('d-m-Y H:i').')', 'Couldnt pull the CentroBill IPs');
            return curl_error($ch);
        }
        curl_close($ch);

        $ips = json_decode($response, true);

        // Create TXT file
        $web_folder = $container->get('kernel')->getRootDir().'/../web/';
        $ips_file = $web_folder.'centrobill_allowed_ips.txt';
        $file = fopen($ips_file, 'w');
        fwrite($file, implode(', ', $ips));
        fclose($file);
        @chmod($ips_file, 0777);
    }
}