<?php

namespace XLabs\CentroBillBundle\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use XLabs\CentroBillBundle\API\CentroBill as CentroBillApi;

class CentroBillExtension extends AbstractExtension
{
    private $container;
    private $xlabs_centrobill_config;

    public function __construct(ContainerInterface $container, $xlabs_centrobill_config)
    {
        $this->container = $container;
        $this->xlabs_centrobill_config  = $xlabs_centrobill_config;
    }
    
    public function getFunctions()
    {
        return array(
            //new TwigFunction('getCentroBillOneTimeChargeUrl', array($this, 'getCentroBillOneTimeChargeUrl')),
            //new TwigFunction('getEpochReactivationUrl', array($this, 'getEpochReactivationUrl')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    /*public function getCentroBillOneTimeChargeUrl($purchaseType, $purchase_id, $centrobill_member_id, $amount, $referrer = false, $referrer_action = false)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if(is_string($user))
        {
            return 'javascript:;';
        }

        $this->container->get(CentroBillApi::class)->getOneTimeChargeUrl(array(
            'product_id' => $request->request->get('join_packages')['product_id'],
            'email' => $request->request->get('email'),
            'metadata' => $aMetadata
        ));

        $params = array(
            'auth_amount' => $amount,
            'member_id' => $epoch_member_id,
            'returnurl' => $this->container->get('router')->generate('xlabs_epoch_response', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'pburl' => $this->container->get('router')->generate('xlabs_epoch_postback', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'x_purchaseType' => $purchaseType,
            'x_purchaseId' => $purchase_id
        );
        if($referrer)
        {
            $params['x_referrerUrl'] = $referrer;
        }
        if($referrer_action)
        {
            $params['x_referrerAction'] = $referrer_action;
        }

        $url = $this->container->get('xlabs_epoch_camcharge_api')->getUrl($params);

        return $url;
    }*/

    /*public function getEpochReactivationUrl($epoch_member_id, $pi_code = false)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if(is_string($user))
        {
            return 'javascript:;';
        }

        $params = array(
            'member_id' => $epoch_member_id,
            'pi_returnurl' => $this->container->get('router')->generate('xlabs_epoch_response', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            'pburl' => $this->container->get('router')->generate('xlabs_epoch_postback', array(), UrlGeneratorInterface::ABSOLUTE_URL),
            //'x_purchaseType' => $purchaseType,
            //'x_purchaseId' => $purchase_id
        );
        if($pi_code)
        {
            $params['pi_code'] = $pi_code;
        }

        $url = $this->container->get('xlabs_epoch_reactivation_api')->getUrl($params);

        return $url;
    }*/
}