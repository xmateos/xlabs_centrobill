<?php

namespace XLabs\CentroBillBundle\API\Exception;

use RuntimeException;

class BaseException extends RuntimeException
{

}