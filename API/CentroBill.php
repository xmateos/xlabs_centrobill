<?php

namespace XLabs\CentroBillBundle\API;

use XLabs\CentroBillBundle\API\Exception\MissingMandatoryParamException;

class CentroBill
{
    private $config;
    private $ch;

    public function __construct($config)
    {
        $this->config = $config;
        $this->ch = curl_init();
    }

    private function checkMandatoryParams($required_params, $sent_params)
    {
        foreach($required_params as $required_param)
        {
            if(!isset($sent_params[$required_param]))
            {
                throw new MissingMandatoryParamException('\''.$required_param.'\' mandatory param is missing.');
            }
        }
    }

    public function getJoinUrl($aParams)
    {
        $required_params = array(
            'product_id',
            'email'
        );
        $this->checkMandatoryParams($required_params, $aParams);
        /*$aParams = array_merge($this->default_params, $params);
        ksort($aParams);*/

        $request_params = array(
            'sku' => array(array(
                /* With SiteId and manual pricepoint */
                /*'siteId' => (string)$this->config['site_id'],
                'price' => array(
                    array(
                        'offset' => '0d',
                        'amount' => 9.99,
                        'currency' => 'USD',
                        'repeat' => false
                    ),
                    array(
                        'offset' => '60d',
                        'amount' => 50,
                        'currency' => 'USD',
                        'repeat' => false
                    ),
                    array(
                        'offset' => '30d',
                        'amount' => 10,
                        'currency' => 'USD',
                        'repeat' => true
                    ),
                ),*/
                /* With ProductId defined at CentroBill end */
                'name' => $aParams['product_id'],
                'url' => array(
                    'redirectUrl' => rtrim($this->config['url'], '/').'/'.trim($this->config['url_prefix'], '/').'/'.trim($this->config['return_url'], '/'),
                    'ipnUrl' => rtrim($this->config['url'], '/').'/'.trim($this->config['url_prefix'], '/').'/'.trim($this->config['postback_url'], '/'),
                    // local test
                    //'ipnUrl' => 'https://www.waitmyturn.com/'.trim($this->config['url_prefix'], '/').'/'.trim($this->config['postback_url'], '/'),
                ),
            )),
            'consumer' => array(
                'email' => $aParams['email']
            ),
            'emailOptions' => array(
                'send' => true
            ),
            'ttl' => $this->config['api']['ttl']
        );
        if(isset($aParams['title']) && $aParams['title'])
        {
            $request_params['sku']['title'] = $aParams['title'];
        }
        if(isset($aParams['name']) && $aParams['name'])
        {
            $request_params['sku']['name'] = $aParams['name'];
        }
        if(isset($aParams['domainName']) && $aParams['domainName'])
        {
            $request_params['sku']['domainName'] = $aParams['domainName'];
        }
        if(isset($aParams['metadata']) && is_array($aParams['metadata']))
        {
            $request_params['metadata'] = $aParams['metadata'];
        }

        curl_setopt_array($this->ch, [
            CURLOPT_URL => $this->config['api']['url'].'/paymentPage',
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($request_params)
        ]);

        $request = $this->doCurl();
        if($request)
        {
            if(isset($request['response']['url']))
            {
                return $request['response']['url'];
            }
        }
        return false;
    }

    public function getOneTimeChargeUrl($aParams)
    {
        $required_params = array(
            'product_id',
            'consumer_id',
            //'email'
        );
        $this->checkMandatoryParams($required_params, $aParams);
        /*$aParams = array_merge($this->default_params, $params);
        ksort($aParams);*/

        $request_params = array(
            'sku' => array(array(
                /* With SiteId and manual pricepoint */
                /*'siteId' => (string)$this->config['site_id'], // this should be set as the biller site id in the CMS
                'price' => array(
                    array(
                        'offset' => '0d',
                        'amount' => 9.99,
                        'currency' => 'USD',
                        'repeat' => false
                    ),
                    array(
                        'offset' => '60d',
                        'amount' => 50,
                        'currency' => 'USD',
                        'repeat' => false
                    ),
                    array(
                        'offset' => '30d',
                        'amount' => 10,
                        'currency' => 'USD',
                        'repeat' => true
                    ),
                ),*/
                /* With ProductId defined at CentroBill end */
                'name' => $aParams['product_id'],
                'url' => array(
                    'redirectUrl' => rtrim($this->config['url'], '/').'/'.trim($this->config['url_prefix'], '/').'/'.trim($this->config['return_url'], '/'),
                    'ipnUrl' => rtrim($this->config['url'], '/').'/'.trim($this->config['url_prefix'], '/').'/'.trim($this->config['postback_url'], '/'),
                    // local test
                    //'ipnUrl' => 'https://www.waitmyturn.com/'.trim($this->config['url_prefix'], '/').'/'.trim($this->config['postback_url'], '/'),
                ),
            )),
            'consumer' => array(
                //'email' => $aParams['email']
                'id' => $aParams['consumer_id']
            ),
            'emailOptions' => array(
                'send' => true
            ),
            'ttl' => $this->config['api']['ttl']
        );
        if(isset($aParams['title']) && $aParams['title'])
        {
            $request_params['sku']['title'] = $aParams['title'];
        }
        if(isset($aParams['name']) && $aParams['name'])
        {
            $request_params['sku']['name'] = $aParams['name'];
        }
        if(isset($aParams['domainName']) && $aParams['domainName'])
        {
            $request_params['sku']['domainName'] = $aParams['domainName'];
        }
        if(isset($aParams['metadata']) && is_array($aParams['metadata']))
        {
            $request_params['metadata'] = $aParams['metadata'];
        }
        $request_params['metadata']['onetime_charge'] = 1; // custom metadata to detect one-time charges

        curl_setopt_array($this->ch, [
            CURLOPT_URL => $this->config['api']['url'].'/paymentPage',
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($request_params)
        ]);

        $request = $this->doCurl();
        if($request)
        {
            if(isset($request['response']['url']))
            {
                return $request['response']['url'];
            }
        }
        return false;
    }

    public function getCancelSubscriptionUrl($aParams)
    {
        $required_params = array(
            'subscription_id'
        );
        $this->checkMandatoryParams($required_params, $aParams);
    }

    public function cancelSubscription($aParams)
    {
        $required_params = array(
            'subscription_id'
        );
        $this->checkMandatoryParams($required_params, $aParams);

        curl_setopt_array($this->ch, [
            CURLOPT_URL => $this->config['api']['url'].'/subscription/'.$aParams['subscription_id'].'/cancel',
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => http_build_query($aParams)
        ]);

        $request = $this->doCurl();
        if($request)
        {
            return $request;
        }
        return false;
    }

    public function reactivateSubscription($aParams)
    {
        $required_params = array(
            'subscription_id'
        );
        $this->checkMandatoryParams($required_params, $aParams);

        curl_setopt_array($this->ch, [
            CURLOPT_URL => $this->config['api']['url'].'/subscription/'.$aParams['subscription_id'].'/recover',
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => http_build_query($aParams)
        ]);

        $request = $this->doCurl();
        if($request)
        {
            return $request;
        }
        return false;
    }

    public function doCurl()
    {
        curl_setopt_array($this->ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => [
                "Authorization: ".$this->config['api']['key'],
                "accept: application/json",
                "content-type: application/json"
            ],
        ]);
        $response = curl_exec($this->ch);
        $httpCode = curl_getinfo($this->ch);
        $err = curl_error($this->ch);
        curl_close($this->ch);
        //dump($response, $httpCode, $err); die;
        if(!$err)
        {
            return [
                'response' => json_decode($response, true),
                'info' => $httpCode,
                'error' => $err
            ];
        }
        return false;
    }

    public function getFeed($aParams)
    {
        $url = 'https://feed.centrobill.com/v2/lookup';

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $url.'?'.http_build_query($aParams),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "Authorization: ".$this->config['api']['key'],
                "accept: application/json",
                "content-type: application/json"
            ],
        ]);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if(!$err)
        {
            return json_decode($response, true);
        }
        //dump($request_params, $response, $httpCode, $err); die;
        return false;
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}

