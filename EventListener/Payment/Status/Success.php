<?php

namespace XLabs\CentroBillBundle\EventListener\Payment\Status;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\CentroBillBundle\Services\Firewall;
use XLabs\CentroBillBundle\Services\Logger as CentroBillLogger;
use XLabs\CentroBillBundle\Request\Request as CentroBillRequest;
use XLabs\CentroBillBundle\Services\MailNotification as CentroBillMailNotification;
use Symfony\Component\HttpFoundation\Response;
use XLabs\CentroBillBundle\Entity\Transaction as TransactionEntity;
use \DateTime;

class Success
{
    private $config;
    private $em;
    private $event_dispatcher;
    private $firewall;
    private $logger;
    private $mail_notification;

    public function __construct($config, EntityManagerInterface $em, EventDispatcherInterface $event_dispatcher, Firewall $firewall, CentroBillLogger $logger, CentroBillMailNotification $mail_notification)
    {
        /*
         * 'event_dispatcher' gets different interfaces depending on the environment, that´s why I don´t set its type
         */
        $this->config = $config;
        $this->em = $em;
        $this->event_dispatcher = $event_dispatcher;
        $this->firewall = $firewall;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
    }
    
    public function onSuccess(Event $event)
    {
        $centrobillRequest = $event->getParams(); // XLabs\CentroBillBundle\Request\Request.php
        $data = $centrobillRequest->getData();

        $this->logger->info('IPN success postback: '.$data['payment']['transactionId']);

        $ipn = new TransactionEntity();
        $ipn->__set('transaction_id', $data['payment']['transactionId']);
        $ipn->__set('transaction_date', DateTime::createFromFormat('Y-m-d H:i:s', $data['payment']['timestamp']['dateTime']));
        foreach($data as $key => $value)
        {
            if(property_exists($ipn, $key))
            {
                $ipn->__set($key, json_encode($value));
            }
        }
        $this->em->persist($ipn);

        $e = false;
        try {
            $this->em->flush();
            $this->logger->info('Transaction '.$data['payment']['transactionId'].' stored successfully.');
            switch(strtolower($data['payment']['action']))
            {
                case 'cancel':
                    $e = new Cancel($centrobillRequest);
                    break;
                case 'charge':
                    $e = new Charge($centrobillRequest);
                    break;
                case 'chargeback':
                    $e = new Chargeback($centrobillRequest);
                    break;
                case 'credit':
                    $e = new Credit($centrobillRequest);
                    break;
                case 'redirect':
                    $e = new Redirect($centrobillRequest);
                    break;
                /*default: // transaction
                    $e = new Transaction($centrobillRequest);
                    break;*/
            }
        } catch (\Exception $exception) {
            // Probably transaction already exists
            //$this->logger->error('Transaction '.$transaction->__get('ets_transaction_id').' not stored: '.$exception->getMessage(), 'postback');
            $this->logger->error('Transaction '.$data['payment']['transactionId'].' not stored: '.$exception->getMessage(), 'postback');
            //dump($exception); die;
        }

        if($e)
        {
            $this->event_dispatcher->dispatch($e::NAME, $e);
            $event_response = $e->getResponse();
            if($event_response)
            {
                $event->setResponse($event_response);
            }
        } else {
            $this->logger->error('Postback failed', 'postback');
        }
    }
}