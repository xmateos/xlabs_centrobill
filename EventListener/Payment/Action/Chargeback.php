<?php

namespace XLabs\CentroBillBundle\EventListener\Payment\Action;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\CentroBillBundle\Services\Firewall;
use XLabs\CentroBillBundle\Services\Logger as CentroBillLogger;
use XLabs\CentroBillBundle\Services\MailNotification as CentroBillMailNotification;

class Chargeback extends Event
{
    private $config;
    private $event_dispatcher;
    private $firewall;
    private $logger;
    private $mail_notification;

    public function __construct($config, EventDispatcherInterface $event_dispatcher, Firewall $firewall, CentroBillLogger $logger, CentroBillMailNotification $mail_notification)
    {
        /*
         * 'event_dispatcher' gets different interfaces depending on the environment, that´s why I don´t set its type
         */
        $this->config = $config;
        $this->event_dispatcher = $event_dispatcher;
        $this->firewall = $firewall;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
    }
    
    public function onChargeback(Event $event)
    {
        $centrobillRequest = $event->getParams(); // XLabs\CentroBillBundle\Request\Request.php
        $data = $centrobillRequest->getData();
    }
}