<?php

namespace XLabs\CentroBillBundle\EventListener\Payment\Action;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\CentroBillBundle\Services\Firewall;
use XLabs\CentroBillBundle\Services\Logger as CentroBillLogger;
use XLabs\CentroBillBundle\Services\MailNotification as CentroBillMailNotification;
use XLabs\CentroBillBundle\Event\Actions\Join;
use XLabs\CentroBillBundle\Event\Actions\Rebill;
use XLabs\CentroBillBundle\Event\Actions\OneTimePurchase;

class Charge extends Event
{
    private $config;
    private $event_dispatcher;
    private $firewall;
    private $logger;
    private $mail_notification;

    public function __construct($config, EventDispatcherInterface $event_dispatcher, Firewall $firewall, CentroBillLogger $logger, CentroBillMailNotification $mail_notification)
    {
        /*
         * 'event_dispatcher' gets different interfaces depending on the environment, that´s why I don´t set its type
         */
        $this->config = $config;
        $this->event_dispatcher = $event_dispatcher;
        $this->firewall = $firewall;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
    }
    
    public function onCharge(Event $event)
    {
        $centrobillRequest = $event->getParams(); // XLabs\CentroBillBundle\Request\Request.php
        $data = $centrobillRequest->getData();

        $e = false;
        if(isset($data['subscription']))
        {
            switch(strtolower($data['payment']['mode']))
            {
                case 'auth':
                case 'sale':
                case 'test':
                    if($data['payment']['status'] == 'success')
                    {
                        switch(true)
                        {
                            case isset($data['subscription']['cycle']) && $data['subscription']['cycle'] == 0:
                                $e = new Join($centrobillRequest);
                                $this->mail_notification->send($centrobillRequest, 'IPN join');
                                break;
                            case isset($data['subscription']['cycle']) && $data['subscription']['cycle'] > 0:
                                $e = new Rebill($centrobillRequest);
                                $this->mail_notification->send($centrobillRequest, 'IPN rebill');
                                break;
                        }
                    }
                    break;
            }
        } elseif(isset($data['metadata']) && isset($data['metadata']['onetime_charge'])) {
            if(isset($data['payment']) && isset($data['payment']['status']) && $data['payment']['status'] == 'success')
            {
                $e = new OneTimePurchase($centrobillRequest);
                $this->mail_notification->send($centrobillRequest, 'IPN one-time purchase');
            }
        } else {
            $this->mail_notification->send($centrobillRequest, 'IPN unknown');
        }

        if($e)
        {
            $this->event_dispatcher->dispatch($e::NAME, $e);
            $this->logger->info('Event '.$e::NAME.' dispatched.');

            $event_response = $e->getResponse();
            if($event_response)
            {
                $event->setResponse($event_response);
            }
        }
    }
}