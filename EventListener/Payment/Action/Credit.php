<?php

namespace XLabs\CentroBillBundle\EventListener\Payment\Action;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\CentroBillBundle\Services\Firewall;
use XLabs\CentroBillBundle\Services\Logger as CentroBillLogger;
use XLabs\CentroBillBundle\Services\MailNotification as CentroBillMailNotification;
use XLabs\CentroBillBundle\Event\Actions\Refund;

class Credit extends Event
{
    private $config;
    private $event_dispatcher;
    private $firewall;
    private $logger;
    private $mail_notification;

    public function __construct($config, EventDispatcherInterface $event_dispatcher, Firewall $firewall, CentroBillLogger $logger, CentroBillMailNotification $mail_notification)
    {
        /*
         * 'event_dispatcher' gets different interfaces depending on the environment, that´s why I don´t set its type
         */
        $this->config = $config;
        $this->event_dispatcher = $event_dispatcher;
        $this->firewall = $firewall;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
    }
    
    public function onCredit(Event $event)
    {
        $centrobillRequest = $event->getParams(); // XLabs\CentroBillBundle\Request\Request.php
        $data = $centrobillRequest->getData();

        $e = false;
        /*if(isset($data['subscription']))
        {*/
            switch(strtolower($data['payment']['mode']))
            {
                case 'refund':
                    switch($data['payment']['status'])
                    {
                        case 'fail':
                        case 'failed':
                            break;
                        case 'pending':
                            break;
                        case 'success':
                            $e = new Refund($centrobillRequest);
                            $this->mail_notification->send($centrobillRequest, 'IPN refund');
                            break;
                    }
                    break;
                case 'void':
                    switch($data['payment']['status'])
                    {
                        case 'fail':
                        case 'failed':
                            break;
                        case 'pending':
                            break;
                        case 'success':
                            break;
                    }
                    break;
            }
        /*} else {
            $this->mail_notification->send($centrobillRequest, 'IPN unknown');
        }*/

        if($e)
        {
            $this->event_dispatcher->dispatch($e::NAME, $e);
            $this->logger->info('Event '.$e::NAME.' dispatched.');

            $event_response = $e->getResponse();
            if($event_response)
            {
                $event->setResponse($event_response);
            }
        }
    }
}