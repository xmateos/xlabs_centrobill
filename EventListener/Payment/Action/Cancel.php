<?php

namespace XLabs\CentroBillBundle\EventListener\Payment\Action;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\CentroBillBundle\Services\Firewall;
use XLabs\CentroBillBundle\Services\Logger as CentroBillLogger;
use XLabs\CentroBillBundle\Services\MailNotification as CentroBillMailNotification;
use XLabs\CentroBillBundle\Event\Actions\Cancel as CancelSubscription;

class Cancel extends Event
{
    private $config;
    private $event_dispatcher;
    private $firewall;
    private $logger;
    private $mail_notification;

    public function __construct($config, EventDispatcherInterface $event_dispatcher, Firewall $firewall, CentroBillLogger $logger, CentroBillMailNotification $mail_notification)
    {
        /*
         * 'event_dispatcher' gets different interfaces depending on the environment, that´s why I don´t set its type
         */
        $this->config = $config;
        $this->event_dispatcher = $event_dispatcher;
        $this->firewall = $firewall;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
    }
    
    public function onCancel(Event $event)
    {
        $centrobillRequest = $event->getParams(); // XLabs\CentroBillBundle\Request\Request.php
        $data = $centrobillRequest->getData();

        $e = false;
        if(isset($data['subscription']))
        {
            // When cancelling from CB customer´s website
            if(isset($data['subscription']['status']) && $data['subscription']['status'] == 'canceled')
            {
                $e = new CancelSubscription($centrobillRequest);
                $this->mail_notification->send($centrobillRequest, 'IPN cancel');
            }
        }

        if($e)
        {
            $this->event_dispatcher->dispatch($e::NAME, $e);
            $this->logger->info('Event '.$e::NAME.' dispatched.');

            $event_response = $e->getResponse();
            if($event_response)
            {
                $event->setResponse($event_response);
            }
        }
    }
}