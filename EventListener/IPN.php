<?php

namespace XLabs\CentroBillBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\CentroBillBundle\Services\Firewall;
use XLabs\CentroBillBundle\Services\Logger as CentroBillLogger;
use XLabs\CentroBillBundle\Request\Request as CentroBillRequest;
use XLabs\CentroBillBundle\Services\MailNotification as CentroBillMailNotification;
use Symfony\Component\HttpFoundation\Response;
use XLabs\CentroBillBundle\Event\Payment\Action\Cancel;
use XLabs\CentroBillBundle\Event\Payment\Action\Charge;
use XLabs\CentroBillBundle\Event\Payment\Action\Chargeback;
use XLabs\CentroBillBundle\Event\Payment\Action\Credit;
use XLabs\CentroBillBundle\Event\Payment\Action\Redirect;
use XLabs\CentroBillBundle\Entity\Transaction as TransactionEntity;
use \DateTime;

class IPN
{
    private $config;
    private $em;
    private $event_dispatcher;
    private $firewall;
    private $logger;
    private $mail_notification;

    public function __construct($config, EntityManagerInterface $em, EventDispatcherInterface $event_dispatcher, Firewall $firewall, CentroBillLogger $logger, CentroBillMailNotification $mail_notification)
    {
        /*
         * 'event_dispatcher' gets different interfaces depending on the environment, that´s why I don´t set its type
         */
        $this->config = $config;
        $this->em = $em;
        $this->event_dispatcher = $event_dispatcher;
        $this->firewall = $firewall;
        $this->logger = $logger;
        $this->mail_notification = $mail_notification;
    }
    
    public function onCentroBillIPN(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if($request->get('_route') == 'xlabs_centrobill_postback')
        {
            $centrobillRequest = new CentroBillRequest($request);
            $content = 'OK unkown';
            if(!empty($centrobillRequest->getData()))
            {
                //$this->mail_notification->send($centrobillRequest, 'Postback'); // for testing only

                $path_prefix = $this->config['url_prefix'];
                $path_prefix = $path_prefix ? $path_prefix : '';
                $postback_url = $this->config['url'].$path_prefix.$this->config['postback_url'].'?mmparams='.urlencode($centrobillRequest->getData(true));

                $data = $centrobillRequest->getData();
                if(!$this->firewall->centrobillIpAllowed())
                {
                    $content = 'IP restricted';
                    $this->logger->error('IP restricted: '.trim($_SERVER['REMOTE_ADDR']).PHP_EOL.$postback_url, 'firewall');
                } else {
                    if(isset($data['payment']))
                    {
                        if($this->isTrustedPostback($centrobillRequest))
                        {
                            $this->logger->info('IPN postback: '.$data['payment']['transactionId']);

                            $ipn = new TransactionEntity();
                            $ipn->__set('transaction_id', $data['payment']['transactionId']);
                            $ipn->__set('transaction_date', DateTime::createFromFormat('Y-m-d H:i:s', $data['payment']['timestamp']['dateTime']));
                            foreach($data as $key => $value)
                            {
                                if(property_exists($ipn, $key))
                                {
                                    $ipn->__set($key, json_encode($value));
                                }
                            }
                            $this->em->persist($ipn);

                            $e = false;
                            try {
                                //$this->em->flush();
                                $this->logger->info('Transaction '.$data['payment']['transactionId'].' stored successfully.');
                                switch(strtolower($data['payment']['action']))
                                {
                                    case 'cancel':
                                        $e = new Cancel($centrobillRequest);
                                        break;
                                    case 'charge':
                                        $e = new Charge($centrobillRequest);
                                        break;
                                    case 'chargeback':
                                        $e = new Chargeback($centrobillRequest);
                                        break;
                                    case 'credit':
                                        $e = new Credit($centrobillRequest);
                                        break;
                                    case 'redirect':
                                        $e = new Redirect($centrobillRequest);
                                        break;
                                }
                            } catch (\Exception $exception) {
                                // Probably transaction already exists
                                //$this->logger->error('Transaction '.$transaction->__get('ets_transaction_id').' not stored: '.$exception->getMessage(), 'postback');
                                $this->logger->error('Transaction '.$data['payment']['transactionId'].' not stored: '.$exception->getMessage(), 'postback');
                                //dump($exception); die;
                            }
                            if($e)
                            {
                                $this->event_dispatcher->dispatch($e::NAME, $e);
                                $event_response = $e->getResponse();
                                $content = $event_response ? $event_response : 'OK'; // force 'OK' response if non is given
                            } else {
                                $this->logger->error('Postback failed', 'postback');
                            }
                        } else {
                            $content = 'x-signature header not matched';
                            $this->logger->error('x-signature header not matched: '.trim($_SERVER['REMOTE_ADDR']).PHP_EOL.$postback_url, 'firewall');
                        }
                    } elseif(isset($data['subscription'])) {
                        // When cancelling from CB customer´s website
                        if(isset($data['subscription']['status']) && $data['subscription']['status'] == 'canceled')
                        {
                            $e = new Cancel($centrobillRequest);
                            $this->event_dispatcher->dispatch($e::NAME, $e);
                            $event_response = $e->getResponse();
                            $content = $event_response ? $event_response : 'OK'; // force 'OK' response if non is given
                        }
                    }
                }
            }

            $response = new Response();
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);
            $response->setContent($content);
            $event->setResponse($response);
        }
    }

    private function isTrustedPostback($centrobillRequest)
    {
        $isCustomPostback = $centrobillRequest->getXSignature() == 'xlabs';
        $data = $centrobillRequest->getData();
        $isTrustedPostback = hash('sha256', $this->config['api']['key'].$data['payment']['transactionId'].$data['payment']['status']) == $centrobillRequest->getXSignature();
        return $isCustomPostback || $isTrustedPostback;
    }
}