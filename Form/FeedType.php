<?php

namespace XLabs\CentroBillBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use XLabs\CentroBillBundle\Entity\FeedRequest;

class FeedType extends AbstractType
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('siteName',ChoiceType::class,array(
                'label' => 'feed.form.site_name',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'choices' => array_combine($this->config['site_name'], $this->config['site_name']),
                'data' => $this->config['site_name'][0],
                'required' => false,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => false,
                'attr' => ['class' => 'form-select form-select-sm']
            ))
            /*->add('dateFrom',SingleDatePickerType::class, array(
                'label' => 'feed.form.from_date',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'year_range' => array(
                    'start' => 2023,
                    'end' => date('Y') + 5
                ),
                'attr' => ['class' => 'form-control form-control-sm']
            ))*/
            ->add('dateFrom', DateType::class, [
                'label' => 'feed.form.from_date',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'widget' => 'single_text',
                'html5' => false,
                'attr' => [
                    'class' => 'form-control form-control-sm datepicker',
                    //'placeholder' => 'From date',
                    'autocomplete' => 'off'
                ]
            ])
            /*->add('dateTo',SingleDatePickerType::class, array(
                'label' => 'feed.form.to_date',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'year_range' => array(
                    'start' => 2023,
                    'end' => date('Y') + 5
                ),
                'attr' => ['class' => 'form-control form-control-sm']
            ))*/
            ->add('dateTo', DateType::class, [
                'label' => 'feed.form.to_date',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'widget' => 'single_text',
                'html5' => false,
                'attr' => [
                    'class' => 'form-control form-control-sm datepicker',
                    //'placeholder' => 'To date',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('orderId',TextType::class, array(
                'label' => 'feed.form.order_id',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'attr' => ['class' => 'form-control form-control-sm']
            ))
            ->add('transactionId',TextType::class, array(
                'label' => 'feed.form.transaction_id',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'attr' => ['class' => 'form-control form-control-sm']
            ))
            ->add('ustas',TextType::class, array(
                'label' => 'feed.form.member_id',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'attr' => ['class' => 'form-control form-control-sm']
            ))
            ->add('uuid',TextType::class, array(
                'label' => 'feed.form.transaction_uuid',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'attr' => ['class' => 'form-control form-control-sm']
            ))
            ->add('status',ChoiceType::class,array(
                'label' => 'feed.form.status.label',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'choices' => array(
                    'feed.form.status.choices.all' => '',
                    'feed.form.status.choices.success' => 'success',
                    'feed.form.status.choices.fail' => 'fail',
                    'feed.form.status.choices.pending' => 'pending',
                ),
                'data' => '',
                'required' => false,
                'expanded' => false,
                'multiple' => false,
                'attr' => ['class' => 'form-select form-select-sm']
            ))
            ->add('mode',ChoiceType::class,array(
                'label' => 'feed.form.mode.label',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'choices' => array(
                    'feed.form.mode.choices.all' => '',
                    'feed.form.mode.choices.sale' => 'sale',
                    'feed.form.mode.choices.auth' => 'auth',
                ),
                'data' => '',
                'required' => false,
                'expanded' => false,
                'multiple' => false,
                'attr' => ['class' => 'form-select form-select-sm']
            ))
            ->add('search_type',ChoiceType::class,array(
                'label' => 'feed.form.search_type.label',
                'translation_domain' => 'centrobill_feed',
                'label_attr' => ['class' => 'form-label'],
                'choices' => array(
                    'feed.form.search_type.choices.local' => 'local',
                    'feed.form.search_type.choices.remote' => 'remote',
                ),
                'data' => 'local',
                'required' => false,
                'expanded' => false,
                'multiple' => false,
                'mapped' => false,
                'placeholder' => false,
                'attr' => ['class' => 'form-select form-select-sm']
            ))
            /*->add('cardLast4Digits',TextType::class, array(
                'label' => 'Last 4 card digits',
                'label_attr' => ['class' => 'form-label'],
                'required' => false,
                'attr' => [
                    'class' => 'form-control form-control-sm',
                    'maxlength' => 4,
                    'pattern' => '[0-9]*'
                ],
            ))*/
            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary btn-sm bg-primary bg-gradient'
                ),
                'label' => 'feed.form.get_results',
                'translation_domain' => 'centrobill_feed'
            ))
            ->add('reset', ResetType::class, array(
                'attr' => array(
                    'class' => 'btn btn-secondary btn-sm bg-secondary bg-gradient'
                ),
                'label' => 'feed.form.clear_filters',
                'translation_domain' => 'centrobill_feed'
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event){
            $form = $event->getForm();
            $feed_request = $event->getData();
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => FeedRequest::class,
            'csrf_protection' => false
        ));
    }

    public function getBlockPrefix()
    {
        return '';
    }
}