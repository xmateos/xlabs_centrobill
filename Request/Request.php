<?php

namespace XLabs\CentroBillBundle\Request;

class Request
{
    private $request;
    private $json_data;
    private $data;
    private $x_signature;

    public function __construct($request)
    {
        $this->request = $request;
        $this->json_data = array();
        if(empty($request->query->all()))
        {
            // original postback
            $this->json_data = $request->getContent();
            $this->x_signature = $request->headers->get('x-signature');
        } else {
            // resent by me (from email)
            $this->json_data = $request->query->get('mmparams');
            $this->x_signature = 'xlabs';
        }
        $this->data = json_decode($this->json_data, true);
    }

    public function __get($name)
    {
        if(array_key_exists($name, $this->data))
        {
            return $this->data[$name];
        }
        return false;
    }

    public function getData($as_json = false)
    {
        return $as_json ? $this->json_data : $this->data;
    }

    public function getXSignature()
    {
        return $this->x_signature;
    }
}