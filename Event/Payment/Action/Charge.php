<?php

namespace XLabs\CentroBillBundle\Event\Payment\Action;

use XLabs\CentroBillBundle\Event\IPN;

class Charge extends IPN
{
    const NAME = 'centrobill.payment.action.charge.event';
}