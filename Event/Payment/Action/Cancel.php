<?php

namespace XLabs\CentroBillBundle\Event\Payment\Action;

use XLabs\CentroBillBundle\Event\IPN;

class Cancel extends IPN
{
    const NAME = 'centrobill.payment.action.cancel.event';
}