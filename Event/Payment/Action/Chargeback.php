<?php

namespace XLabs\CentroBillBundle\Event\Payment\Action;

use XLabs\CentroBillBundle\Event\IPN;

class Chargeback extends IPN
{
    const NAME = 'centrobill.payment.action.chargeback.event';
}