<?php

namespace XLabs\CentroBillBundle\Event\Payment\Action;

use XLabs\CentroBillBundle\Event\IPN;

class Credit extends IPN
{
    const NAME = 'centrobill.payment.action.credit.event';
}