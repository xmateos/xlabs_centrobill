<?php

namespace XLabs\CentroBillBundle\Event\Payment\Action;

use XLabs\CentroBillBundle\Event\IPN;

class Redirect extends IPN
{
    const NAME = 'centrobill.payment.action.redirect.event';
}