<?php

namespace XLabs\CentroBillBundle\Event\Payment\Status;

use XLabs\CentroBillBundle\Event\IPN;

class Fail extends IPN
{
    const NAME = 'centrobill.payment.status.fail.event';
}