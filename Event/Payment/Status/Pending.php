<?php

namespace XLabs\CentroBillBundle\Event\Payment\Status;

use XLabs\CentroBillBundle\Event\IPN;

class Pending extends IPN
{
    const NAME = 'centrobill.payment.status.pending.event';
}