<?php

namespace XLabs\CentroBillBundle\Event\Payment\Status;

use XLabs\CentroBillBundle\Event\IPN;

class Success extends IPN
{
    const NAME = 'centrobill.payment.status.success.event';
}