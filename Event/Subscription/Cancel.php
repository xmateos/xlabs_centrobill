<?php

namespace XLabs\CentroBillBundle\Event\Subscription;

use XLabs\CentroBillBundle\Event\IPN;

class Cancel extends IPN
{
    const NAME = 'centrobill.subscription.cancel.event';
}