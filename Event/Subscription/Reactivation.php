<?php

namespace XLabs\CentroBillBundle\Event\Subscription;

use XLabs\CentroBillBundle\Event\IPN;

class Reactivation extends IPN
{
    const NAME = 'centrobill.subscription.reactivation.event';
}