<?php

namespace XLabs\CentroBillBundle\Event\Actions;

use XLabs\CentroBillBundle\Event\IPN;

class Cancel extends IPN
{
    const NAME = 'centrobill.cancel.event';
}