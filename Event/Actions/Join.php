<?php

namespace XLabs\CentroBillBundle\Event\Actions;

use XLabs\CentroBillBundle\Event\IPN;

class Join extends IPN
{
    const NAME = 'centrobill.join.event';
}