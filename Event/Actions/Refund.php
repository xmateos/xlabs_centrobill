<?php

namespace XLabs\CentroBillBundle\Event\Actions;

use XLabs\CentroBillBundle\Event\IPN;

class Refund extends IPN
{
    const NAME = 'centrobill.refund.event';
}