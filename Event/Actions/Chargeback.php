<?php

namespace XLabs\CentroBillBundle\Event\Actions;

use XLabs\CentroBillBundle\Event\IPN;

class Chargeback extends IPN
{
    const NAME = 'centrobill.chargeback.event';
}