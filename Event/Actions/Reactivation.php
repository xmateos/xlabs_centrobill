<?php

namespace XLabs\CentroBillBundle\Event\Actions;

use XLabs\CentroBillBundle\Event\IPN;

class Reactivation extends IPN
{
    const NAME = 'centrobill.reactivation.event';
}