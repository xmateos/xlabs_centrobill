<?php

namespace XLabs\CentroBillBundle\Event\Actions;

use XLabs\CentroBillBundle\Event\IPN;

class OneTimePurchase extends IPN
{
    const NAME = 'centrobill.onetime_purchase.event';
}