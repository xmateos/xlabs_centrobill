<?php

namespace XLabs\CentroBillBundle\Event\Actions;

use XLabs\CentroBillBundle\Event\IPN;

class TrialToFull extends IPN
{
    const NAME = 'centrobill.trialtofull.event';
}