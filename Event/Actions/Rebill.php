<?php

namespace XLabs\CentroBillBundle\Event\Actions;

use XLabs\CentroBillBundle\Event\IPN;

class Rebill extends IPN
{
    const NAME = 'centrobill.rebill.event';
}