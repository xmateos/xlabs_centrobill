<?php

namespace XLabs\CentroBillBundle\Event\Response;

use XLabs\CentroBillBundle\Event\IPN;

class Success extends IPN
{
    const NAME = 'centrobill.response_success.event';
}