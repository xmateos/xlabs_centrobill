<?php

namespace XLabs\CentroBillBundle\Event\Response;

use XLabs\CentroBillBundle\Event\IPN;

class Failed extends IPN
{
    const NAME = 'centrobill.response_failed.event';
}