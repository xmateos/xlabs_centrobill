<?php

namespace XLabs\CentroBillBundle\Event\Feed;

use Symfony\Component\EventDispatcher\Event;

class Update extends Event
{
    const NAME = 'centrobill.feed.updated.event';

    protected $params;

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($name)
    {
        return array_key_exists($name, $this->params) ? $this->params[$name] : false;
    }
}