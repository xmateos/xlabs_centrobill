<?php

namespace XLabs\CentroBillBundle\Entity;

use \ReflectionClass;
use \DateTime;

class FeedRequest
{
    private $dateFrom; // Format: Y-m-d
    private $dateTo; // Format: Y-m-d
    private $limit; // Per page limit
    private $page; // Page number
    private $orderId; // Order Id
    private $transactionId; // Transaction Id
    private $lastTransactionId; // Transaction Id
    private $ustas; // Consumer Id
    private $systemId; // System Id
    private $uuid; // Transaction uuid
    private $lastUuid; // Transaction uuid
    private $siteName; // Site name
    private $fields; // Selected fields. Comma separated string, with 'dot' notation.
    private $ignored; // Ignored fields. Comma separated string, with 'dot' notation.
    private $status; // Transaction status
    private $mode; // Transaction mode
    private $cardLast4Digits; // Card last 4 digits
    private $externalTransactionId; // Biller transaction ID
    private $gatewayAmount; // Transaction amount
    private $gatewayCurrency; // Transaction currency
    private $gatewayPricePercentDiff; // Price percent diff used for lower and upper bound (10$ with diff 2% = amount will be between 9.8 & 10.2)
    private $loadAll; // Parameter to skip pagination & load full feed
    private $paymentType; // Transaction currency
    private $prevTransactionId; // Prev transaction ID
    private $cardBin; // Card BIN (A bank identification number)

    public function __construct($data = false)
    {
        if($data)
        {
            foreach($data as $key => $val)
            {
                $this->__set($key, $val);
            }
        }
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function toArray()
    {
        $array = [];
        $reflectionClass = new \ReflectionClass($this);
        foreach($reflectionClass->getProperties() as $property)
        {
            $propertyName = $property->getName();
            $array[$propertyName] = $this->__get($propertyName);
            /*$getter = 'get' . ucfirst($propertyName);

            if (method_exists($this, $getter)) {
                $array[$propertyName] = $this->$getter();
            }*/
        }
        return $array;
    }

    public function toCleanArray()
    {
        $a = $this->toArray();
        foreach($a as $key => $val)
        {
            if(is_null($val) || $val == '')
            {
                unset($a[$key]);
            } else {
                switch(true)
                {
                    case $val instanceof DateTime:
                        $a[$key] = $val->format('Y-m-d');
                        break;
                }
            }
        }
        return $a;
    }
}

