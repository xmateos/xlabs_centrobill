<?php

namespace XLabs\CentroBillBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Table(name="CentroBillFeed")
 * @ORM\Entity(repositoryClass="XLabs\CentroBillBundle\Repository\FeedRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Feed
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255)
     */
    private $id;

    /*
     * @ORM\PrePersist
     */
    /*public function generateId()
    {
        $hashData = $this->payment.$this->consumer.$this->subscription.$this->metadata;
        $this->id = md5($hashData);
    }*/

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $payment;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $consumer;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $subscription;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $metadata;

    public function __construct($data)
    {
        foreach($data as $key => $obj)
        {
            $this->__set($key, json_encode($obj));
        }
        // remove event time from the subscription object for the hash generation
        $subscription = json_decode($this->subscription, true);
        unset($subscription['eventTime']);
        $hashData = $this->payment.$this->consumer.json_encode($subscription).$this->metadata;
        $this->id = md5($hashData);
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __call($name, $arguments)
    {
        if($name === '__get' && count($arguments) === 2)
        {
            $name = $arguments[0];
            $as_json = $arguments[1];
            return isset($this->data[$name]) ? ($as_json ? $this->$name : json_decode($this->$name, true)) : false;
        }
    }
    public function __set($name, $value)
    {
        /*if(strpos($name, 'extra ') !== false)
        {
            $name = str_replace('extra ', '', $name);
        }
        // GMT date and time fields
        $aFields = array('transtime', 'reactivationtimestamp');
        if(in_array($name, $aFields))
        {
            $value = DateTime::createFromFormat('m/d/Y H:i:s A', $value);
        }
        // mm/dd/yyyy fields
        $aFields = array('nextbilldate', 'lastbilldate');
        if(in_array($name, $aFields))
        {
            $value = DateTime::createFromFormat('m/d/Y', $value);
        }*/
        $this->$name = $value;
    }
}

