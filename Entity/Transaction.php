<?php

namespace XLabs\CentroBillBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Table(name="CentroBillTransaction")
 * @ORM\Entity(repositoryClass="XLabs\CentroBillBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /*
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="transaction_id", type="bigint")
     * @ORM\Id
     */
    private $transaction_id;

    /**
     * @ORM\Column(name="transaction_date", type="datetime", nullable=true)
     */
    private $transaction_date;

    public function getPublishdate()
    {
        return $this->transaction_date;
    }

    public function setPublishdate($transaction_date)
    {
        $this->transaction_date = $transaction_date;
    }

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $payment;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $consumer;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $subscription;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $metadata;

    public function __construct()
    {
        //$this->transaction_date = new DateTime();
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __call($name, $arguments)
    {
        if($name === '__get' && count($arguments) === 2)
        {
            $name = $arguments[0];
            $as_json = $arguments[1];
            return isset($this->data[$name]) ? ($as_json ? $this->$name : json_decode($this->$name, true)) : false;
        }
    }
    public function __set($name, $value)
    {
        /*if(strpos($name, 'extra ') !== false)
        {
            $name = str_replace('extra ', '', $name);
        }
        // GMT date and time fields
        $aFields = array('transtime', 'reactivationtimestamp');
        if(in_array($name, $aFields))
        {
            $value = DateTime::createFromFormat('m/d/Y H:i:s A', $value);
        }
        // mm/dd/yyyy fields
        $aFields = array('nextbilldate', 'lastbilldate');
        if(in_array($name, $aFields))
        {
            $value = DateTime::createFromFormat('m/d/Y', $value);
        }*/
        $this->$name = $value;
    }
}

