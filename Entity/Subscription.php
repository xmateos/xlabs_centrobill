<?php

namespace XLabs\CentroBillBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/*
 * @ORM\Table(name="CentroBillSubscription")
 * @ORM\Entity(repositoryClass="XLabs\CentroBillBundle\Repository\SubscriptionRepository")
 */
class Subscription
{
    const STATUS_ACTIVE = 1;
    const STATUS_CANCELED = 0;

    /*
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    //private $id;

    /**
     * @ORM\Id
     * @ORM\Column(name="subscription_id", type="bigint")
     */
    private $subscription_id;

    public function getSubscriptionId()
    {
        return $this->subscription_id;
    }

    public function setSubscriptionId($subscription_id)
    {
        $this->subscription_id = $subscription_id;
    }

    /**
     * @ORM\Column(name="member_id", type="bigint")
     */
    private $member_id;

    public function getMemberId()
    {
        return $this->member_id;
    }

    public function setMemberId($member_id)
    {
        $this->member_id = $member_id;
    }

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }
}

