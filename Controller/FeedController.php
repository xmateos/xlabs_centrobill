<?php

namespace XLabs\CentroBillBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use XLabs\CentroBillBundle\Form\FeedType;
use XLabs\CentroBillBundle\Entity\FeedRequest;
use XLabs\CentroBillBundle\API\CentroBill as CentroBillApi;
use XLabs\MMAdminBundle\Annotations as XLabsMMAdmin;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\CentroBillBundle\Entity\Feed;

/**
 * @XLabsMMAdmin\isProtected
 */
class FeedController extends Controller
{
    /**
     * @Route("/", name="xlabs_centrobill_feed", options={"expose"=true})
     */
    public function indexAction(Request $request, CentroBillApi $centroBill)
    {
        /*$event = new \XLabs\CentroBillBundle\Event\Feed\Update([
            'user' => 'me'
        ]);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch($event::NAME, $event);*/

        $feed = new FeedRequest();
        $form = $this->createForm(FeedType::class, $feed, array());
        // it won´t be submitted; jquery intercepts the call in template
        /*$form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $params = $feed->toCleanArray();
            $data = $centroBill->getFeed($params);
            dump($feed, $data); die;
        }*/
        return $this->render('XLabsCentroBillBundle:feed:index.html.twig', array(
            'form' => $form->createView(),
            'api_key' => $this->getParameter('xlabs_centrobill_config')['api']['key']
        ));
    }

    /**
     * @Route("/load", name="xlabs_centrobill_feed_load", options={"expose"=true})
     */
    public function load(Request $request, EntityManagerInterface $em, CentroBillApi $centroBill)
    {
        $dt_params = $request->request->all();
        $feed_params = $dt_params['feed_params'];

        //dump($dt_params); die;

        $draw = $dt_params['draw'];
        $row = $dt_params['start'];
        $rowperpage = (int)$dt_params['length']; // Rows display per page
        $columnIndex = $dt_params['order'][0]['column']; // Column index
        $columnName = $dt_params['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $dt_params['order'][0]['dir']; // asc or desc
        $searchValue = addslashes($dt_params['search']['value']); // Search value

        // Search
        /*$searchQuery = '';
        if($searchValue != '')
        {
            $searchQuery = ' and (wr.name like "%'.$searchValue.'%") ';
        }*/

        // getFeed should be called here, merging parameters from datatables
        if($rowperpage < 0) // show all
        {
            $feed_params['loadAll'] = 1;
        } else {
            $feed_params['limit'] = $rowperpage;
            $feed_params['page'] = ($row / $rowperpage) + 1;
        }

        switch($feed_params['search_type'])
        {
            case 'local':
                unset($feed_params['search_type']);
                $feed = new FeedRequest($feed_params);
                $params = $feed->toCleanArray();
                $params['offset'] = $dt_params['start']; // for pagination
                $data = $em->getRepository(Feed::class)->getFeed($params);
                // Total number of records without filtering
                $totalRecords = $data['total'];
                // Total number of record with filtering
                $totalRecordwithFilter = $data['total'];
                unset($data['total']);
                break;
            case 'remote':
                unset($feed_params['search_type']);
                $feed = new FeedRequest($feed_params);
                $data = $centroBill->getFeed($feed->toCleanArray());
                if(isset($data['error']))
                {
                    return new JsonResponse(array(
                        "draw" => intval($draw),
                        "iTotalRecords" => 0,
                        "iTotalDisplayRecords" => 0,
                        "aaData" => [],
                        'error' => $data['error']
                    ));
                }
                if(isset($data['message']))
                {
                    return new JsonResponse(array(
                        "draw" => intval($draw),
                        "iTotalRecords" => 0,
                        "iTotalDisplayRecords" => 0,
                        "aaData" => [],
                        'error' => $data['message']
                    ));
                }
                // Total number of records without filtering
                $totalRecords = isset($data['meta']) ? $data['meta']['total'] : 0;
                // Total number of record with filtering
                $totalRecordwithFilter = isset($data['meta']) ? $data['meta']['total'] : 0;
                break;
        }
        //dump($feed, $feed->toCleanArray(), $data); die;

        // Fetch records
        $aData = array();
        $config = $this->getParameter('xlabs_centrobill_config');
        if(isset($data['data']))
        {
            foreach($data['data'] as $record)
            {
                // IPN
                $path_prefix = $config['url_prefix'];
                $path_prefix = $path_prefix ? $path_prefix : '';
                $ipn_url = $config['url'].$path_prefix.$config['postback_url'].'?mmparams='.urlencode(json_encode($record));

                $aData[] = [
                    //'email' => isset($record['consumer']) ? $record['consumer']['email'] : '~',
                    'payment' => json_encode(isset($record['payment']) ? $record['payment'] : array()),
                    'consumer' => json_encode(isset($record['consumer']) ? $record['consumer'] : array()),
                    'subscription' => json_encode(isset($record['subscription']) ? $record['subscription'] : array()),
                    'metadata' => json_encode(isset($record['metadata']) ? $record['metadata'] : array()),
                    'all' => [
                        'record' => json_encode($record),
                        'ipn' => $ipn_url
                    ]
                ];
            }
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $aData
        );

        return new JsonResponse($response);
    }
}
