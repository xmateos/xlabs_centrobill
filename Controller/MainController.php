<?php

namespace XLabs\CentroBillBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use XLabs\CentroBillBundle\Event\Response\Success;
use XLabs\CentroBillBundle\Event\Response\Failed;
use XLabs\CentroBillBundle\API\DeclineCodes;
use XLabs\CentroBillBundle\API\CentroBill as CentroBillApi;
use XLabs\CentroBillBundle\Event\Subscription\Reactivation;
use XLabs\CentroBillBundle\Event\Subscription\Cancel;

class MainController extends Controller
{
    public function responseAction(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        $params = array();
        parse_str($request->getQueryString(), $params);
        $is_approved = isset($params['is_approved']) && $params['is_approved'];

        $status = $is_approved ? 'SUCCESS' : 'ERROR';
        $message = $is_approved ? '' : ('Error code '.$params['cb_code'].': '.DeclineCodes::getDescriptiveError($params['cb_code']));
        //$e = $is_approved ? new Success($epochRequest) : new Failed($epochRequest);
        //$eventDispatcher->dispatch($e::NAME, $e);

        return $this->render('XLabsCentroBillBundle:response:response.html.twig', array(
            'status' => $status,
            'message' => is_null($message) ? '' : $message
        ));
    }

    public function oneTimePurchaseAction(Request $request, CentroBillApi $centrobill_api)
    {
        $data = urldecode($request->get('d')); // json
        if($data)
        {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $data = json_decode($data, true);
            $url = $centrobill_api->getOneTimeChargeUrl(array(
                'product_id' => $data['product_id'],
                'consumer_id' => $data['member_id'],
                'metadata' => array(
                    'purchaseType' => $data['purchaseType'],
                    'purchaseId' => $data['purchaseId'],
                    'user_id' => $user->getId()
                )
            ));
            if($url)
            {
                return $this->redirect($url);
            } else {
                $this->get('session')->getFlashBag()->add('swal', array(
                    'type' => 'error',
                    'title' => '',
                    'body' => 'The payment could not be generated. Please contact support.',
                ));
                return $this->redirect($request->headers->get('referer'));
            }
        }
        throw new NotFoundHttpException('No data found.');
    }

    /**
     * @Route("/cancel/{subscription_id}", name="xlabs_centrobill_cancel_subscription", options={"expose"=true})
     */
    public function cancelSubscriptionAction(Request $request, CentroBillApi $centrobill_api, EventDispatcherInterface $eventDispatcher, RouterInterface $router, $subscription_id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $response = $centrobill_api->cancelSubscription(array(
            'subscription_id' => $subscription_id
        ));
        $e = new Cancel([
            'user' => $user,
            'response' => $response
        ]);
        $eventDispatcher->dispatch($e::NAME, $e);
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/reactivate/{subscription_id}", name="xlabs_centrobill_reactivate_subscription", options={"expose"=true})
     */
    public function reactivateSubscriptionAction(Request $request, CentroBillApi $centrobill_api, EventDispatcherInterface $eventDispatcher, RouterInterface $router, $subscription_id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $response = $centrobill_api->reactivateSubscription(array(
            'subscription_id' => $subscription_id
        ));
        $e = new Reactivation([
            'user' => $user,
            'response' => $response
        ]);
        $eventDispatcher->dispatch($e::NAME, $e);
        if($e->getResponse())
        {
            return $e->getResponse();
        }
        return $this->redirect($request->headers->get('referer'));
    }
}
