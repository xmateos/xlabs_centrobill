<?php

namespace XLabs\CentroBillBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use \DateTime;

class CentroBillLoader extends Loader
{
    private $config;
    private $loaded = false;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "xlabs_centrobill_routing" loader twice');
        }

        $routes = new RouteCollection();

        $importedRoutes = $this->import('@XLabsCentroBillBundle/Resources/config/routing.yml', 'yaml');
        $routes->addCollection($importedRoutes);

        $aRoutes = array();

        // CentroBill URL prefix
        $path_prefix = $this->config['url_prefix'];
        $path_prefix = $path_prefix ? $path_prefix : '';

        // Join form
        /*$path = $path_prefix.$this->config['join_url'];
        $defaults = array(
            '_controller' => 'XLabsCentroBillBundle:Main:join',
        );
        //$requirements = array(
        //    'parameter' => '\d+',
        //);
        //$options = array(
        //    'expose' => 'true'
        //);
        $requirements = array();
        //$route = new Route($path, $defaults, $requirements, $options);
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_centrobill_join';
        $aRoutes[$routeName] = $route;*/

        // Route for postbacks
        $path = $path_prefix.$this->config['postback_url'];
        $defaults = array(
            '_controller' => 'XLabsCentroBillBundle:Main:postback',
        );
        /*$requirements = array(
            'parameter' => '\d+',
        );*/
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_centrobill_postback';
        $aRoutes[$routeName] = $route;

        // Route for return urls
        $path = $path_prefix.$this->config['return_url'];
        $defaults = array(
            '_controller' => 'XLabsCentroBillBundle:Main:response',
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_centrobill_response';
        $aRoutes[$routeName] = $route;

        // Route for one-time purchases
        $path = $path_prefix.$this->config['onetime_purchase_url'];
        $defaults = array(
            '_controller' => 'XLabsCentroBillBundle:Main:oneTimePurchase',
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_centrobill_onetime_purchase';
        $aRoutes[$routeName] = $route;

        // Route for feed
        /*$path = $path_prefix.$this->config['feed_url'];
        $defaults = array(
            '_controller' => 'XLabsCentroBillBundle:Feed:index',
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_centrobill_feed';
        $aRoutes[$routeName] = $route;*/

        foreach($aRoutes as $routeName => $route)
        {
            $routes->add($routeName, $route);
        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'xlabs_centrobill_routing' === $type;
    }
}