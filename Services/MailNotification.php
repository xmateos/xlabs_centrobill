<?php

namespace XLabs\CentroBillBundle\Services;

use XLabs\CentroBillBundle\Request\Request as CentroBillRequest;

class MailNotification
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function send(CentroBillRequest $centrobillRequest, $subject = false)
    {
        $aData = $centrobillRequest->getData();

        if($this->config['mail_notifications']['enabled'])
        {
            $path_prefix = $this->config['url_prefix'];
            $path_prefix = $path_prefix ? $path_prefix : '';
            $postback_url = $this->config['url'].$path_prefix.$this->config['postback_url'].'?mmparams='.urlencode(json_encode($aData));

            // Subject
            $nSubject = 'CentroBill postback: ';
            $nSubject .= $this->config['mail_notifications']['subject'] ? $this->config['mail_notifications']['subject'].' ' : '';
            $nSubject .= $subject ? $subject : '';

            // Body
            $nBody = '<br /><span style="text-align: center; background-color: #4DB24C; padding: 10px 15px; border-radius: 5px;"><a href="'.$postback_url.'" style="color: #fff; font-size: 14px; letter-spacing: 1px; text-decoration: none; text-shadow: 0px 2px 2px #4db24c; font-family: Arial, Helvetica, sans-serif;">Click me to resend postback</a></span><br /><br />';
            $nBody .= '<br /><b>Parameters</b>:<br />';
            $nBody .= $this->print_array_recursive($aData);
            $nBody .= '<br /><br /><b>Link</b>:<br />'.urldecode($postback_url);

            // Headers
            $nHeaders  = 'MIME-Version: 1.0' . "\r\n";
            $nHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            if($this->config['mail_notifications']['from'] != '')
            {
                $nHeaders .= 'From: '.$this->config['mail_notifications']['from'] . "\r\n";
            }

            if(count($this->config['mail_notifications']['destinataries']))
            {
                foreach($this->config['mail_notifications']['destinataries'] as $recipient)
                {
                    mail($recipient, $nSubject, $nBody, $nHeaders);
                }
            }
        }
    }

    private function print_array_recursive($array, $indent = 30)
    {
        $result = "";
        foreach($array as $key => $value)
        {
            if(is_array($value))
            {
                $result .= '<span style="margin-left: '.$indent.'px;">'.$key.':<span/><br />';
                $result .= $this->print_array_recursive($value, $indent + 30);
            } else {
                $result .= '<span style="margin-left: '.$indent.'px;">'.$key.': <i>'.$value.'</i><span/><br />';
            }
        }
        return $result;
    }
}