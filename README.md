An CentroBill wrapper.

## Installation ##
Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/centrobillbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\CentroBillBundle\XLabsCentroBillBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
x_labs_centrobill:
    resource: .
    type: xlabs_centrobill_routing
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml

x_labs_centro_bill:
    url: 'https://www.bikinifanatics.com'
    site_name: ['bikinifanatics.com'] # CentroBill site name array
    url_prefix: /centrobill
    postback_url: /postback
    return_url: /response
    onetime_purchase_url: /oneclick
    api:
        key: XXXXXXXXX
        url: 'https://api.centrobill.com'
        ttl: 1200
    logging:
        enabled: true
        location: '%kernel.logs_dir%/'
    allowed_ips: ['xx.xx.xx.xx']
    mail_notifications:
        enabled: true
        subject: 'BikiniFanatics'
        from: 'centrobillpostback@bikinifanatics.com'
        destinataries: ["xavi.mateos@manicamedia.com"]
```

## Update allowed IPs
``` php
php bin/console xlabs:centrobill:update_ips
```

## Update feed
Set a cronjob as much frequent as possible, maybe every 2-4h; Trial2Full members will depend on this feed.
``` php
php bin/console xlabs:centrobill:feed
```