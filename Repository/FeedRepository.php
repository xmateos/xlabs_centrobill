<?php

namespace XLabs\CentroBillBundle\Repository;

use Doctrine\ORM\EntityRepository;
use XLabs\CentroBillBundle\Entity\Feed;
use \DateTime;

class FeedRepository extends EntityRepository
{
    public function getFeed($params)
    {
        /*
          "siteName" => "waitmyturn.com"
          "dateFrom" => ""
          "dateTo" => ""
          "ustas" => ""
          "transactionId" => ""
          "uuid" => ""
          "orderId" => ""
          "status" => ""
          "mode" => ""
          "limit" => 5
          "page" => 1
         */
        //dump($params); die;
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb_total = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('f')
            ->from(Feed::class, 'f')
        ;
        $qb_total
            ->select($qb_total->expr()->countDistinct('f.id'))
            ->from(Feed::class, 'f')
        ;
        if(isset($params['dateFrom']))
        {
            $dateFrom = new DateTime($params['dateFrom']);
            $qb
                ->andWhere(
                    "DATE(JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.timestamp.dateTime'))) >= :dateFrom"
                )
                ->setParameter('dateFrom', $dateFrom->format('Y-m-d'))
            ;
            $qb_total
                ->andWhere(
                    "DATE(JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.timestamp.dateTime'))) >= :dateFrom"
                )
                ->setParameter('dateFrom', $dateFrom->format('Y-m-d'))
            ;
        }
        if(isset($params['dateTo']))
        {
            $dateTo = new DateTime($params['dateTo']);
            $qb
                ->andWhere(
                    "DATE(JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.timestamp.dateTime'))) <= :dateTo"
                )
                ->setParameter('dateTo', $dateTo->format('Y-m-d'))
            ;
            $qb_total
                ->andWhere(
                    "DATE(JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.timestamp.dateTime'))) <= :dateTo"
                )
                ->setParameter('dateTo', $dateTo->format('Y-m-d'))
            ;
        }
        if(isset($params['ustas']))
        {
            $qb
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.consumer), '$.id')) = :ustas"
                )
                ->setParameter('ustas', $params['ustas'])
            ;
            $qb_total
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.consumer), '$.id')) = :ustas"
                )
                ->setParameter('ustas', $params['ustas'])
            ;
        }
        if(isset($params['transactionId']))
        {
            $qb
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.transactionId')) = :transactionId"
                )
                ->setParameter('transactionId', $params['transactionId'])
            ;
            $qb_total
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.transactionId')) = :transactionId"
                )
                ->setParameter('transactionId', $params['transactionId'])
            ;
        }
        if(isset($params['uuid']))
        {
            $qb
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.uuid')) = :uuid"
                )
                ->setParameter('uuid', $params['uuid'])
            ;
            $qb_total
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.uuid')) = :uuid"
                )
                ->setParameter('uuid', $params['uuid'])
            ;
        }
        if(isset($params['orderId']))
        {
            $qb
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.orderId')) = :orderId"
                )
                ->setParameter('orderId', $params['orderId'])
            ;
            $qb_total
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.orderId')) = :orderId"
                )
                ->setParameter('orderId', $params['orderId'])
            ;
        }
        if(isset($params['status']))
        {
            $qb
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.status')) = :status"
                )
                ->setParameter('status', $params['status'])
            ;
            $qb_total
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.status')) = :status"
                )
                ->setParameter('status', $params['status'])
            ;
        }
        if(isset($params['mode']))
        {
            $qb
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.mode')) = :mode"
                )
                ->setParameter('mode', $params['mode'])
            ;
            $qb_total
                ->andWhere(
                    "JSON_UNQUOTE(JSON_EXTRACT(JSON_UNQUOTE(f.payment), '$.mode')) = :mode"
                )
                ->setParameter('mode', $params['mode'])
            ;
        }
        if(!isset($params['loadAll']))
        {
            if(isset($params['limit']))
            {
                $qb
                    ->setMaxResults($params['limit'])
                    ->setFirstResult($params['offset'])
                ;
            }
        }
        $results = $qb->getQuery()->getArrayResult();
        //dump($results); die;

        array_walk($results, function (&$value) {
            unset($value['id']);
            foreach($value as $key => $val)
            {
                $value[$key] = json_decode($val, true);
            }
        });

        return [
            'data' => $results,
            'total' => $qb_total->getQuery()->getSingleScalarResult()
        ];
    }
}